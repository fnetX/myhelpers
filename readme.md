# My helpers
This repo contains many little helper scripts I use for my linux desktop. It's main purpose is for syncing them between machines, it's secondary purpose is to make the stuff available for anyone to use or remix ...

I'm currently working on improving them and making them ready for general usage, thus sharing them step by step over here ... Feel free to ask for improvements or commit them yourself.

## Whats in here?
The probably most interesting scripts are
### mysync 
A script to share a local folder to a remote machine (via rsync and ssh) and wait for file edits. Useful when developing locally but deploying everything to a remote machine.

Configuration can be made via a .mysyncrc in the current working directory, you can launch it with `mysync setup` to have a somewhat interactive guide for the settings. 

It uses inotifywait to wait for file edits without syncing, fallback via custom sleep timer possible. 

### mywcenter
It scales a window to 90 percent (or something else) on primary monitor and centers it with some additional space to the upper screen border, because my menu bar is up there :joy: 